#COMNANDOS DOCKER/VAGRANT/VB BASEADO VIDEOS YB

```bash
$ docker system info #verificar estado do docker
$ docker search #procura images ex: docker search debian
$ docker image pull alpine #baixar somente a imagem
$ docker container run -dit --name alpine --hostname teste alpine #criando container
$ docker container run -dit -p 22:22 --name debian-ssh --hostname debian-ssh ssh #subir container para teste ssh/buildar a imagem antes
$ echo 'teste' > /tmp/teste.log && docker container cp /tmp/teste.log alpine:tmp # copiando arquivo para dentro do container
$ docker container exec alpine ls -l /tmp # conferindo o arquivo
$ docker container run --rm -it hello-world #rm remove quando terminar
$ docker ps -a #temos tambem container/volume/image ls -a
$ docker ps -aq #somente ID
$ docker rm -f ID #remove o container forcando
$ docker rm $(docker ps -aq) #remove todos containers com subshell 
$ ssh ip172-18-0-165-c9apb9433d5g00d0fqog@direct.labs.play-with-docker.com #comando para acessar do seu terminal com ssh o play with docker
$ docker container run -it -p 8080:8080 pengbai/docker-supermario #rodar super mario no play with docker
$ docker container run --rm -it echo-container #container é removido quando sai
$ docker login -u douglastos #logar ho docker hub, e usar o logout para sair
$ docker history debian #verificar todas camadas criadas (historico)
$ docker inspect debian #verificar inspeciona mais profundo as camadas
$ docker container commit debian_container debian-imagem-http #cria imagem a partir do container
$ docker image save debian-imagem-http -o debian-imagem-http.tar #criar backup da imagem
$ docker image load -i debian-imagem-http.tar #restore da imagem
$ docker image build -t echo-container . #buildar uma imagem a partir do arquivo Dockerfile
$ docker image build --no-cache -t echo-container . #buildar uma imagem a partir do arquivo Dockerfile sem cache
$ docker image ls | head -n1 && docker images | grep debian #fitro para comparacao de images (purto linux)
$ docker image ls | egrep "REPO|debian" #fitro para comparacao de images (purto linux)

```

## COMANDOS NO DOCKERFILE

```dockerfile
FROM        #-> diz qual é a origem da imagem (imagem base)
COPY        #-> copia arquivos de path para o container
RUN         #-> executa comandos dentro do container
ADD         #-> Quase igual copy, mais aceita URL e alterar permissoes 
EXPOSE      #-> Expoe uma porta (só convenincia no Dockerfile)
ENTRYPOINT  #-> ponto de entrada do container (o que mantem o container vivo)
CMD         #-> Argumentos para o entreypoint

# DICA 1 : A ORDEM IMPORTA PARA O CACHE
# DICA 2 : COPY mas especifico para limitar a quebra de cache
# DICA 3 : identifique as instruçoes que podem ser AGRUPADAS.
# DICA 4 : remova dependecias desnecessarias ex usar "--no-install-recommends"
# DICA 5 : remover cache do gerenciador de pacotes
# DICA 6 : usar imagem oficial
# DICA 7 : utilize tags mais especificas
# DICA 8 : procure por falvors minimos (procurar por slim "debian GNU->libc" ou alpine musl->libc)
```

#VOLUMES
```dockerfile
VOLUME # verificar qual o volume para usar com docker system info

alterar o drive storage # /etc/docker/daemon.json

#criando um volume bind -> -v origem:destino
$ docker container run -dit --name server -v /srv:/srv debian

#ciando volume anonimo -> -v destino (nesse modo ele cria um volume com um hash)
$ docker container run -dit --name server -v /volume debian

#criando volume nomeado -> -v (sem PATH, somente o nome)
$ docker container run -dit --name server -v volume:/volume debian

#criando volume com mount:
$ docker container run -dit --name server2 --mount source=volume2,target=/volume2 debian

#copiando arquivo com comando docker container cp
$ docker container cp ~/dockerfiles webserver:webdata

#trazendo conteudo de um container rodando
$ docker container run -dit --volumes-from webserver --name volumetest debian
```

#NERWORKS

```bash

#recursos de redes utilizados pelo docker

$ # veth -> Virtual ethernet 
$ # bridge -> roteamento do host pra o container
$ # iptables -> firewall linux
$ #dokcer por padrao nao publica portas para o mundo
$ #comando para publicar portas
$ docker container run -dit --name web -p 7070:80 nginx
$ #  -p / --publish ORIGEM:DESTINO -> host:container ex: 7070:80 o acesso sera localhost:7070
$ # -p 7070:80 -> PEGUE TODA CONEXAO NA PORTA 7070 DO HOST E DIRECIONE PARA PORTA 80 DO CONTAINER
$ # DRIVERS DE REDE \
$ #  - bridge \
$ #  - host \
$ #  - overlay \
$ #  - macvlan \
$ #  - none \
$ #  - plugins de drivers de rede
$ # REDE BRIDGE -> resolucao DNS + conecte e desconecte containers
$ docker container run -dit --name web --hostname webteste --network bridge -p 8080:80 nginx #comando para setar rede bridge
$ # comando para verificar como funciona esse comando na rede
$ curl localhost:8080 # verifica o que esta respondendo na porta 8080
$ sudo ss -ntpl | grep 80 
$ sudo iptables -nL 
$ docker container run -dit --name webhost --hostname webteste --network host nginx #comando para subir container com rede host (sem roteamento usando Ip da maquina local)
$ docker container run -dit --name semrede --network none alpine ash #comando para subir container com rede none entrepoynt ash
$ docker container exec semrede ip link show #comando para verificar so tem o loopback
$ # REDE BRIDGE DEFAULT NAO RESOLVE NOME
$ # USER-DEFINED BRIDGE NETWORK (RESOLVE DNS) 
$ # BENEFICIO DE USAR (USER-DEFINED BRIDGE)
$ # - DNS AUTOMATICO 
$ # - MELHOR ISOLAMENTO 
$ # - CONECTAR E DESCONECTAR ON-THE-FLY
$ # - CONFUGRACAO PERSONALIZADAS
$ docker network create --driver bridge --subnet 172.20.0.0/16 dca-lan #COMANDO PARA CRIAR REDE USER DEFINED
$ docker container run -dit --name servidor-dcalan -h servidor-dcalan --network dca-lan debian #criar container para testar a rede criada
$ docker exec servidor-dcalan ping -c 4 client-dcalan #testetano dns da rede
PING client-dcalan (172.20.0.3) 56(84) bytes of data.
64 bytes from client-dcalan.dca-lan (172.20.0.3): icmp_seq=1 ttl=64 time=0.129 ms
64 bytes from client-dcalan.dca-lan (172.20.0.3): icmp_seq=2 ttl=64 time=0.147 ms
64 bytes from client-dcalan.dca-lan (172.20.0.3): icmp_seq=3 ttl=64 time=0.139 ms
64 bytes from client-dcalan.dca-lan (172.20.0.3): icmp_seq=4 ttl=64 time=0.149 ms
$ docker network disconnect dca-lan client-dcalan #comando para desconectar o cabo de rede
$ docker network connect --ip 172.20.0.200 dca-lan client-dcalan #reconectando o container a rede com IP de preferencia.
$ docker exec -it servidor-dcalan ping -c 4 client-dcalan
PING client-dcalan (172.20.0.200) 56(84) bytes of data.
64 bytes from client-dcalan.dca-lan (172.20.0.200): icmp_seq=1 ttl=64 time=0.161 ms
64 bytes from client-dcalan.dca-lan (172.20.0.200): icmp_seq=2 ttl=64 time=0.118 ms
64 bytes from client-dcalan.dca-lan (172.20.0.200): icmp_seq=3 ttl=64 time=0.119 ms
64 bytes from client-dcalan.dca-lan (172.20.0.200): icmp_seq=4 ttl=64 time=0.699 ms

--- client-dcalan ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.118/0.274/0.699/0.245 ms
$
```

## DOCKER SWARM

``` bash
# COMPOSICAO DO CLUSTER

# MASTER -> MANAGER
# NO1 -> WORKER
# NO2 -: WORKER
# RAFT CONSENSUS -> UTILIZAR NO MAXIMO ATE 7 NOS MANAGER
# [1] -> 3 -> 5 -> [7]
# start no no1 comando
$ docker swarm init --advertise-addr (IP) #nó atual
Swarm initialized: current node (v7pffbu3tgltd16k6gjhguzrm) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2fv3z869jsh4rh8y8d7ho7y7mqkuxdc8ym6k1hztb0sqaw61g9-7cikh9rauhlzxw5djgmbb34xz 192.168.0.8:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
v7pffbu3tgltd16k6gjhguzrm *   node1      Ready     Active         Leader           20.10.0
n8xy83vb3a8cikfwi4tzfmk8q     node2      Ready     Active                          20.10.0
2g3n77ow9deebvtcg28vvosj0     node3      Ready     Active                          20.10.0
$ docker node promote node3 #promovendo o node3 para manager
Node node3 promoted to a manager in the swarm.
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
v7pffbu3tgltd16k6gjhguzrm *   node1      Ready     Active         Leader           20.10.0
n8xy83vb3a8cikfwi4tzfmk8q     node2      Ready     Active                          20.10.0
2g3n77ow9deebvtcg28vvosj0     node3      Ready     Active         Reachable        20.10.0
$ docker node demote node3 #promovendo o node3 a worker
Manager node3 demoted in the swarm.
$ 

#CONCEITOS 
# SERVICES -> SERVIÇO -> ESTADO DESEJADO
# TASKS -> TAREFA

# SERVICE             -> TASK    -> CONTAINER (RESULTADO)
#  3 REPLICAS NGINX   -> nginx.1 -> nginx:latest
#                     -> nginx.2 -> nginx:latest
#                     -> nginx.3 -> nginx:latest
# SERVICOS REPLICADOS X GLOBAIS
# GLOBAL -. RODA EM TODOS OS NOS
# REPLICADO ->  RODA EM UMA QUANTIDADE DETERMINADA

# PROXIMO PASSO COLOCAR MAQUINA REGISTER
#subir 4 nos e executar esse comando em cada um deles 
$ echo '{ "insecure-registries" : ["registry.docker-dca.example:5000"] }' | sudo tee /etc/docker/daemon.json ; sudo systemctl restart docker
#comando é para que cada aceita maquina registry como segura
$ docker container run -dit --name registry -p 5000:5000 registry:2
$ docker pull alpine
$ docker image tag alpine registry.docker-dca.example:5000/alpine
$ docker push registry.docker-dca.example:5000/alpine
# SUBINDO A PRIMEIRA TASK
$ docker service create --name webserver registry.docker-dca.example:5000/nginx
$ docker service ps webserver
ID             NAME          IMAGE                                           NODE                        DESIRED STATE   CURRENT STATE            ERROR     PORTS
75qgrmfaonco   webserver.1   registry.docker-dca.example:5000/nginx:latest   node02.docker-dca.example   Running         Running 38 seconds ago
$ docker service update --publish-add 80 webserver # add uma porta ao servico
$ docker service ps webserver
ID             NAME              IMAGE                                           NODE                        DESIRED STATE   CURRENT STATE                 ERROR     PORTS
s2p653283zkq   webserver.1       registry.docker-dca.example:5000/nginx:latest   node01.docker-dca.example   Running         Running about a minute ago
75qgrmfaonco    \_ webserver.1   registry.docker-dca.example:5000/nginx:latest   node02.docker-dca.example   Shutdown        Shutdown about a minute ago
$ docker service ls
ID             NAME        MODE         REPLICAS   IMAGE                                           PORTS
6oqd0gcg9h1n   webserver   replicated   1/1        registry.docker-dca.example:5000/nginx:latest   *:30000->80/tcp
$ docker service inspect webserver --pretty
$ docker service create --name pingtest alpine ping google.com
$ docker service update --replicas 10 pingtest
#TRABALHANDO COM SECRET
$ echo "123456789" | docker secret create senha_db -
$ docker secret inspect senha_db --pretty
$ docker stack deploy -c webserver.yml webserver-nginx #create deploy com docker-compose
Creating network webserver-nginx_default
Creating service webserver-nginx_webserver
$ docker stack ps webserver-nginx
ID             NAME                          IMAGE          NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
cupf7wdtrswl   webserver-nginx_webserver.1   nginx:latest   node1     Running         Running 2 minutes ago
$ docker node ls
$ docker node update --label-add os=ubuntu18.04 node1 #ADD LABELS PARA INSERIR NO DOCKER-COMPOSE NO DEPLOY
$ docker node update --label-add location=us-east-1 node1
$ docker node update --label-add disk=ssd node1
$ docker node inspect --pretty node1 #INSPECIONANDO AS LABELS
ID:                     o13hbwb1v2cwsytlynkn551r3
Labels:
 - disk=ssd
 - location=us-east-1
 - os=ubuntu18.04..

$ 
######### docker-compose executado#######
version: '3.9'

services:
  webserver:
    image: nginx:latest
    hostname: webserver
    ports:
      - 8080:80
    deploy:
      mode: replicated
      replicas: 5
      placement:
        constraints:
          - node.role==worker
          - node.labels.os==ubuntu18.04
          - node.labels.location==us-east-1
      restart_policy:
        condition: on-failure
###########################################  
```

## KUBERNETS - K8S

``` bash
#usando minikube
# depois de instalado vamos aos comandos

$ minikube version
$ minikube start --driver=docker #usando docker
$ kubectl get nodes #kubectl + verbo + recursos + opcoes
$ mkdir k8s/ && cd k8s/
$ vim pod.yml #conteudo esta no livro
$ kubectl apply -f pod.yml
$  kubectl get pods
NAME   READY   STATUS    RESTARTS   AGE
demo   1/1     Running   0          23m
$ kubectl logs -f demo #verifica o pod pigando 8.8.8.8
$ kubectl delete -f pod.yml #ou
$ kubectl delete pod demo
$ vim multi-container.yml #conteudo esta no livro   
$ kubectl apply -f multi-container.yml
$ kubectl get pods
NAME              READY   STATUS     RESTARTS   AGE
multi-container   1/2     NotReady   0
$ kubectl logs multi-container -c ngnix-containe
$ kubectl logs multi-container -c debian-container
$ kubectl exec -it multi-container -c nginx-container -- /bin/bash
root@multi-container:/# curl localhost
Hello from the debian container
root@multi-container:/#
exit
$ vim nginx-pod.yml
$ kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          8s
$ kubectl apply -f nginx-svc.yml
$ kubectl get services
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1     <none>        443/TCP   164m
nginx-dca    ClusterIP   10.99.214.0   <none>        80/TCP    10s
$ kubectl describe service nginx-dca #comando para verificar o servico
$ minikube ssh
docker@minikube:~$ curl 172.17.0.3:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
docker@minikube:~$
exit
$ vim nginx-svc-nodeport.yml
$ kubectl get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          174m
nginx-dca    NodePort    10.99.171.48   <none>        8080:30033/TCP   15s
$ kubectl run --rm -it alpine --image=alpine --restart=Never -- ash
$ apk update; apk add curl
$ curl nginx-dca:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
/ #
exit
$
$ minikube service --url nginx-dca #enderedo para acesso ao browser
$ curl $(minikube service --url nginx-dca)
$ kubectl delete -f nginx-svc-nodeport.yml
$ kubectl delete -f -f nginx-pod.yml
$ vim nginx-deploy.yml # criando um deployment
$ kubectl get deployments
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           9s
$ kubectl describe deployment nginx-deployment #verificando o deployment
$ kubectl delete pods -l app=nginx-dca #usando filtro com label
$ kubectl get all -l app=nginx-dca #usando filtro com label
$ kubectl delete deploy nginx-deployment #delete do deploy
$ vim configmap.yml
$ kubectl apply -f configmap.yml
$ kubectl get configmap
NAME               DATA   AGE
configmap-app1     3      9s
kube-root-ca.crt   1      3h46m
$ vim pod-configmap.yml
$ kubectl apply -f pod-configmap.yml 
$ kubectl exec -it pod/app1 -- ash
/ # ls /etc/configs
initial_refresh_value      ui_properties_file_name    user-interface.properties
/ # cat /etc/configs/initial_refresh_value
4/ # cat /etc/configs/ui_properties_file_name
user-interface.properties/ # cat /etc/configs/user-interface.properties
color.good=green
color.bad=red
/ #
exit
$ kubectl delete pod/app1
$ vim secret.yml
$ kubectl apply -f secret.yml
$ kubectl get secrets
NAME          TYPE                       DATA   AGE
senha-mysql   kubernetes.io/basic-auth   2      8s
$ vim pod-secret.yml
$ kubectl apply -f pod-secret.yml
$ kubectl exec -it mysql-db -- mysql -u root -pmudar@123
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.0.31 MySQL Community Server - GPL

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.00 sec)

mysql> exit
Bye
$ kubectl delete pod/mysql-db
$ #criando volumes 
$ minikube ssh
$ sudo mkdir /mnt/dados{1..3}
$ sudo sh -c "echo 'Kubernetes Storage Dados 1' > /mnt/dados1/index.html"
$ sudo sh -c "echo 'Kubernetes Storage Dados 2' > /mnt/dados2/index.html"
$ sudo sh -c "echo 'Kubernetes Storage Dados 3' > /mnt/dados3/index.html"
$ ls -lR /mnt
$ exit
$ vim pv.yml
$ kubectl apply -f pv.yml
persistentvolume/pv10m created
persistentvolume/pv200m created
persistentvolume/pv1g created
$ kubectl get pv
NAME     CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pv10m    10Mi       RWO            Retain           Available           manual                  43s
pv1g     1Gi        RWO            Retain           Available           manual                  43s
pv200m   200Mi      RWO            Retain           Available           manual                  43s
$ kubectl apply -f pvc.yml
persistentvolumeclaim/pvc100m created
persistentvolumeclaim/pvc700m created
$ kubectl get pvc
NAME      STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc100m   Bound    pv200m   200Mi      RWO            manual         17s
pvc700m   Bound    pv1g     1Gi        RWO            manual         17s
$ kubectl get pv
NAME     CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM             STORAGECLASS   REASON   AGE
pv10m    10Mi       RWO            Retain           Available                     manual                  36m
pv1g     1Gi        RWO            Retain           Bound       default/pvc700m   manual                  36m
pv200m   200Mi      RWO            Retain           Bound       default/pvc100m   manual                  36m
$ vim webserver.yml
$ kubectl apply -f webserver.yml
$ kubectl exec -it webserver -- /bin/bash
root@webserver:/# curl localhost
Kubernetes Storage Dados 2
root@webserver:/# 
exit
$ 
```

